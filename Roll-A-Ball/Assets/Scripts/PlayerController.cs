using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;
using System.Threading;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public float speed = 0;
    public float gravity = -9.18f;

    public Transform groundCheck;
    public float groundDistance = 0.1f;
    public LayerMask groundMask;
    public float jumpHeight = 3f;
    bool isGrounded;

    public TextMeshProUGUI countText;
    public GameObject winTextObject;
    public GameObject loseTextObject;

    private Rigidbody rb;
    Vector3 velocity;
    private int count;
    private float movementX;
    private float movementY;
    private float movementZ;
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;

        SetCountText();
        winTextObject.SetActive(false);
        loseTextObject.SetActive(false);
    }

    void OnMove(InputValue movementValue) 
    {
        Vector2 movementVector = movementValue.Get<Vector2>();

        movementX = movementVector.x;
        movementY = movementVector.y;
    }

    void FixedUpdate() 
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if(isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }

        velocity.y += gravity * Time.deltaTime;
        rb.AddForce(velocity * Time.deltaTime);


        Vector3 movement = new Vector3(movementX, 0.0f, movementY);

        rb.AddForce(movement * speed);
    }

    private void OnTriggerEnter(Collider other) 
    {
        if(other.gameObject.CompareTag("PickUp")) 
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }

        if(other.gameObject.CompareTag("Enemy")) 
        {
            other.gameObject.SetActive(false);
            SetLoseText();
        }
        if(other.gameObject.CompareTag("Jump")) 
        {
            Vector3 jumpMove = new Vector3(0.0f, 100f, 0.0f);
            rb.AddForce(jumpMove * speed);
        }
    }
    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();

        if (count >= 3)
        {
            winTextObject.SetActive(true);
            Thread.Sleep(5000);
            SceneManager.LoadScene("Minigame");
        }
    }
    void SetLoseText()
    {
        loseTextObject.SetActive(true);
        Thread.Sleep(5000);
        SceneManager.LoadScene("Minigame");
    }

    void OnJump()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
        Debug.Log(isGrounded);
        if(isGrounded)
        {
            rb.AddForce(Vector2.up * jumpHeight, ForceMode.Impulse);
        }
    }
}
